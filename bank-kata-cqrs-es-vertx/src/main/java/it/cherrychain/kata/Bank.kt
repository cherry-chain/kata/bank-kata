package it.cherrychain.kata

import io.vertx.core.AbstractVerticle
import io.vertx.core.Promise
import io.vertx.core.Vertx
import io.vertx.core.buffer.Buffer
import io.vertx.core.eventbus.MessageCodec
import io.vertx.core.json.Json
import it.cherrychain.kata.domain.*
import it.cherrychain.kata.view.*
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import kotlin.reflect.KClass

var formatter: DateTimeFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy")


class Bank(private val domain:Domain, private val view:View):AbstractVerticle() {
    override fun start(startPromise: Promise<Void>) {
        vertx.deployVerticle(domain).compose { vertx.deployVerticle(view) }.onSuccess { startPromise.complete() }
    }
}

class Domain(private val accounts: Accounts):AbstractVerticle() {
    override fun start(startPromise: Promise<Void>) {

        vertx.eventBus().consumer("money deposited", DepositMoneyHandler(accounts))
        vertx.eventBus().consumer("money withdrawn", WithdrawMoneyHandler(accounts))

        startPromise.complete()
    }
}

class View(private val statements : Statements, private val deposits: Deposits):AbstractVerticle() {
    override fun start(startPromise: Promise<Void>) {
        vertx.eventBus().consumer("money deposited", MoneyDepositedHandler(statements))
        vertx.eventBus().consumer("money deposited", TotalMoneyDepositedHandler(deposits))
        vertx.eventBus().consumer("money withdrawn", MoneyWithdrawnHandler(statements))
        startPromise.complete()
    }
}

/**
 * Esempio di come dover scrivere un comando o evento
 */
@JvmRecord
data class Command(val value: Int)
@JvmRecord
data class DomainEvent(val value: Int, val data: LocalDate)


fun main() {
    val vertx = Vertx.vertx()

    vertx.eventBus()
        .registerDefaultCodec(Command::class.java, RecordCodec.of(Command::class))
        .registerDefaultCodec(DomainEvent::class.java, RecordCodec.of(DomainEvent::class))
        .registerDefaultCodec(DepositMoney::class.java, RecordCodec.of(DepositMoney::class))
        .registerDefaultCodec(WithdrawMoney::class.java, RecordCodec.of(WithdrawMoney::class))
        .registerDefaultCodec(MoneyDeposited::class.java, RecordCodec.of(MoneyDeposited::class))
        .registerDefaultCodec(MoneyWithdrawn::class.java, RecordCodec.of(MoneyWithdrawn::class))

    val eventStore = EventStore(mutableListOf(), vertx)
    val accounts = Accounts(eventStore)
    val statements = Statements(mutableListOf())
    val deposits = Deposits(mutableListOf())

    val domain = Domain(accounts)
    val view = View(statements, deposits)


    vertx.deployVerticle(Bank(domain, view)).onSuccess {
        println("applicazione avviata!")
        vertx.eventBus().send("paga bonifico", DepositMoney(500))
    }


}








class RecordCodec<RECORD : Record>(private val type: Class<RECORD>, private val name: String, private val systemCodecID: Byte) : MessageCodec<RECORD, RECORD> {
    override fun encodeToWire(buffer: Buffer, record: RECORD) {
        buffer.appendBuffer(Json.encodeToBuffer(record))
    }

    override fun decodeFromWire(i: Int, buffer: Buffer): RECORD = buffer.toJsonObject().mapTo(type)

    override fun name() = name

    override fun systemCodecID() = systemCodecID

    override fun transform(record: RECORD): RECORD = record

    companion object {
        @JvmStatic
        fun <RECORD : Record> of(type: KClass<RECORD>): RecordCodec<RECORD> {
            return RecordCodec(type.java, type.java.canonicalName, (-1).toByte())
        }
    }
}