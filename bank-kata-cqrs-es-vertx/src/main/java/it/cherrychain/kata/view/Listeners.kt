package it.cherrychain.kata.view

import io.vertx.core.Handler
import io.vertx.core.eventbus.Message
import it.cherrychain.kata.domain.MoneyDeposited
import it.cherrychain.kata.domain.MoneyWithdrawn

class MoneyDepositedHandler(private val statements: Statements) : Handler<Message<MoneyDeposited>> {
    override fun handle(message: Message<MoneyDeposited>) {

        statements.save(message.body().depositedAt, message.body().depositedAmount)
            .onSuccess { }
            .onFailure { }
    }

}


class TotalMoneyDepositedHandler(private val deposits: Deposits) : Handler<Message<MoneyDeposited>> {
    override fun handle(message: Message<MoneyDeposited>) {

        deposits.save(message.body().depositedAt, message.body().depositedAmount)
            .onSuccess { }
            .onFailure { }
    }

}



class MoneyWithdrawnHandler(private val statements: Statements) : Handler<Message<MoneyWithdrawn>> {
    override fun handle(message: Message<MoneyWithdrawn>) {

            statements.save(message.body().withdrawnAt, -message.body().withdrawnAmount)
                .onSuccess {  }
                .onFailure {  }
    }

}