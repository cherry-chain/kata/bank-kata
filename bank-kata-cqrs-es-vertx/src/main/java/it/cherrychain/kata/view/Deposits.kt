package it.cherrychain.kata.view

import io.vertx.core.Future
import java.time.LocalDateTime

data class Deposit(val depositedAt: LocalDateTime, val moneyDeposited: Int)
class Deposits(private val depositList : MutableList<Deposit>) {
    fun save(depositedAt: LocalDateTime, depositedAmount: Int): Future<Deposit> {


        val deposit = Deposit(depositedAt, depositedAmount)
        depositList += deposit
        return Future.succeededFuture(deposit)
    }

    fun findAll(): Future<List<Deposit>> = Future.succeededFuture(depositList.map { it })
    fun findTotalDeposit(): Future<Int>? {
        return findAll().map{ depositList ->
            depositList.sumOf { it.moneyDeposited }
        }
    }


}