package it.cherrychain.kata.view

import io.vertx.core.Future
import java.time.LocalDateTime

data class Statement(val eventDate: LocalDateTime, val amount: Int, val balance: Int)


class Statements(private var statementList :MutableList<Statement>) {

    fun findAll(): Future<List<Statement>> = Future.succeededFuture(statementList.map { it })

    fun save(eventDate: LocalDateTime, amount: Int):Future<Statement> {
        val statementFuture = findAll().map { allStatements ->
            allStatements.sumOf { it.amount?:0 } + amount
        }
            .map { balance -> Statement(eventDate, amount, balance) }

        statementFuture.map { statement -> statementList += statement}

        return statementFuture
    }

}