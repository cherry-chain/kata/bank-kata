package it.cherrychain.kata.domain

import io.vertx.core.Future
import io.vertx.core.Vertx
import java.time.LocalDateTime

@JvmRecord
data class MoneyDeposited(val depositedAmount: Int, val depositedAt: LocalDateTime = LocalDateTime.now())
@JvmRecord
data class MoneyWithdrawn(val withdrawnAmount: Int, val withdrawnAt: LocalDateTime = LocalDateTime.now())

@JvmRecord
data class Event(val domainEvent: Any, val createdAt: LocalDateTime = LocalDateTime.now())

class EventStore(private var eventList :List<Event> = mutableListOf<Event>(), private val vertx: Vertx) {
    fun findAll(): Future<List<Event>> = Future.succeededFuture(eventList.map { it })

    fun save(address:String, domainEvent: Any): Future<Event> {
        val eventToBeSaved = Event(domainEvent)
        eventList += eventToBeSaved
        vertx.eventBus().send(address, domainEvent)
        return Future.succeededFuture(eventToBeSaved)
    }
}