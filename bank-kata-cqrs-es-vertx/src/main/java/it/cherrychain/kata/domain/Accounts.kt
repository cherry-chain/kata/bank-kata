package it.cherrychain.kata.domain

import io.vertx.core.Future


data class Account(val balance: Int) {
    companion object {
        @JvmStatic
        fun fromBalance(balance: Int) = Account(balance)
    }
}

class Accounts(private val eventStore: EventStore) {
    fun generateAccount(): Future<Account> =
        eventStore.findAll()
            .map { allEvents ->
                allEvents.sumOf { storedEvent ->
                    when (storedEvent.domainEvent) {
                        is MoneyDeposited -> storedEvent.domainEvent.depositedAmount
                        is MoneyWithdrawn -> -storedEvent.domainEvent.withdrawnAmount
                        else -> 0
                    }
                }
            }
            .map(Account::fromBalance)

    fun save(event: MoneyDeposited): Future<Unit> = eventStore.save("money deposited", event).mapEmpty()

    fun save(event: MoneyWithdrawn): Future<Unit> = eventStore.save("money withdrawn", event).mapEmpty()
}