package it.cherrychain.kata.domain

import io.vertx.core.Handler
import io.vertx.core.eventbus.Message


@JvmRecord
data class DepositMoney(val moneyToDeposit: Int)
@JvmRecord
data class WithdrawMoney(val moneyToWithdraw: Int)

class DepositMoneyHandler(private val accounts:Accounts) : Handler<Message<DepositMoney>> {
    override fun handle(message: Message<DepositMoney>) {
        val moneyDeposited = MoneyDeposited(message.body().moneyToDeposit)
        accounts.save(moneyDeposited)
            .onSuccess {  }
            .onFailure {  }
    }
}

class WithdrawMoneyHandler(private val accounts:Accounts) : Handler<Message<WithdrawMoney>> {
    override fun handle(message: Message<WithdrawMoney>) {
        accounts.generateAccount()
            .map { account ->
                val balance = account.balance

                if (balance >= message.body().moneyToWithdraw) {
                    val moneyWithdrawn = MoneyWithdrawn(message.body().moneyToWithdraw)
                    accounts.save(moneyWithdrawn)
                }
            }
    }

}