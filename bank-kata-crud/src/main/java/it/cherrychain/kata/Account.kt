package it.cherrychain.kata

import java.time.LocalDate
import java.time.format.DateTimeFormatter


var formatter: DateTimeFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy")

class Transactions {
    val transactionList = mutableListOf<Transaction>()
    fun save(localDate: LocalDate, amount: Int, balance: Int): Transaction {
        val newTransaction = Transaction(localDate, amount, balance)
        transactionList += newTransaction

        return newTransaction
    }

    fun findAll():List<Transaction> {
        val allTransactionList : List<Transaction> = transactionList.map { it }
        return allTransactionList
    }

}

class Statement(private val transactions: Transactions) {
    fun print():String {
        val transactionList = transactions.findAll()
        var statement = "Date          Amount    Balance\n"
        for (transaction in transactionList) {
            val sign = if (transaction.amount>0) "+" else ""
            statement += transaction.localDate.format(formatter)+"     $sign${transaction.amount}       ${transaction.balance}\n"
        }
        println(statement)

        return statement
    }

}
data class Transaction(val localDate: LocalDate, val amount:Int , val balance:Int)

class Account(private var money: Int = 0,
              private val transactions: Transactions,
              private val statement:Statement
) {

    fun deposit(moneyToDeposit:Int): Transaction {
        money += moneyToDeposit

        val dateOfDeposit:LocalDate = LocalDate.now()
        return transactions.save(dateOfDeposit, moneyToDeposit, money)
    }

    fun withdraw(moneyToWithdraw:Int): Transaction {
        money -= moneyToWithdraw

        val dateOfWithdrawal:LocalDate = LocalDate.now()
        return transactions.save(dateOfWithdrawal, -moneyToWithdraw, money)
    }

    fun printStatement(): String {
        return statement.print()
    }
}