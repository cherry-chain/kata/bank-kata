package it.cherrychain.kata

import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import java.time.LocalDate


class AccountTest {
    private val transactions = mockk<Transactions>()
    private val statement = mockk<Statement>()

    @Test
    fun `should deposit some money`() {
        val localDate = LocalDate.now()
        val amount = 500
        val balance = 900

        val expectedTransaction = Transaction(localDate, amount, balance)

        every { transactions.save(localDate, amount, balance) } returns expectedTransaction

        val account:Account = Account(money=400, transactions = transactions, statement = statement)
        val moneyToDeposit:Int = 500

        Assertions.assertThat(account.deposit(moneyToDeposit)).isEqualTo(expectedTransaction)

        verify { transactions.save(localDate, amount, balance) }
    }


    @Test
    fun `should withdraw some money`() {
        val localDate = LocalDate.now()
        val amount = 100
        val balance = 300

        val expectedTransaction = Transaction(localDate, -amount, balance)
        every { transactions.save(localDate, -amount, balance) } returns expectedTransaction

        val account:Account = Account(money=400, transactions = transactions, statement = statement)
        val moneyToWithdraw:Int = 100



        Assertions.assertThat(account.withdraw(moneyToWithdraw)).isEqualTo(expectedTransaction)

        verify { transactions.save(localDate, -amount, balance) }
    }


    @Test
    fun `should print statement`() {

        val localDate = LocalDate.now()
        val moneyToDeposit :Int = 500
        val moneyToWithdraw :Int = 100
        val balanceAfterDeposit :Int = 500
        val balanceAfterWithdrawal :Int = 400

        val expectedDepositTransaction = Transaction(localDate, moneyToDeposit, balanceAfterDeposit)
        val expectedWithdrawalTransaction = Transaction(localDate, -moneyToWithdraw, balanceAfterWithdrawal)
        val expectedStatement :String =
            "Date          Amount    Balance\n" + localDate.format(formatter) + "     +$moneyToDeposit       500\n" + localDate.format(
                formatter) + "     -$moneyToWithdraw       400\n"

        every { transactions.save(localDate, moneyToDeposit, balanceAfterDeposit) } returns expectedDepositTransaction
        every { transactions.save(localDate, -moneyToWithdraw, balanceAfterWithdrawal) } returns expectedWithdrawalTransaction
        every { statement.print() } returns expectedStatement

        val account :Account = Account(transactions= transactions, statement = statement)

        account.deposit(moneyToDeposit)
        account.withdraw(moneyToWithdraw)

        account.printStatement()

        verify { statement.print() }
        Assertions.assertThat(statement.print()).isEqualTo(expectedStatement)

    }
}