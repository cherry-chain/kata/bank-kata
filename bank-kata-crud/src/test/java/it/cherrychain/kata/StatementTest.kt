package it.cherrychain.kata

import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.assertj.core.api.Assertions

import org.junit.jupiter.api.Test
import java.time.LocalDate

class StatementTest {

    private val transactions = mockk<Transactions>()
    @Test
    fun `should print statement`() {
        val localDate = LocalDate.now()
        val amount = 500
        val balance = 500
        val expectedTransactionList = listOf<Transaction>(Transaction(localDate = localDate, amount = amount, balance = balance))

        every { transactions.findAll() } returns expectedTransactionList

        val statement = Statement(transactions)

        val statementString = statement.print()

        val expectedStatementString = "Date          Amount    Balance\n" + localDate.format(formatter) + "     +$amount       $balance\n"

        verify { transactions.findAll() }

        Assertions.assertThat(statementString).isEqualTo(expectedStatementString)
    }
}