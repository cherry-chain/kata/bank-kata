package it.cherrychain.kata

import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import java.time.LocalDate


class AccountIntegrationTest {

    @Test
    fun `should print statement`() {

        val localDate = LocalDate.now()
        val moneyToDeposit :Int = 500
        val moneyToWithdraw :Int = 100

        val expectedStatement :String =
            "Date          Amount    Balance\n" + localDate.format(formatter) + "     +$moneyToDeposit       500\n" + localDate.format(
                formatter) + "     -$moneyToWithdraw       400\n"


        val transactions = Transactions()
        val statement = Statement(transactions)
        val account :Account = Account(transactions= transactions, statement = statement)

        account.deposit(moneyToDeposit)
        account.withdraw(moneyToWithdraw)

        Assertions.assertThat(account.printStatement()).isEqualTo(expectedStatement)

    }
}