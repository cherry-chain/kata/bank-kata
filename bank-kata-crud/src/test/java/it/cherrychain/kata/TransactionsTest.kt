package it.cherrychain.kata

import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import java.time.LocalDate
import kotlin.test.assertContentEquals

class TransactionsTest {

    @Test
    fun `should save transaction`() {
        val localDate = LocalDate.now()
        val amount = 500
        val balance = 600
        val expectedTransaction = Transaction(localDate, amount, balance)

        val transactions = Transactions()

        assertEquals(expectedTransaction,transactions.save(localDate, amount, balance))
    }

    @Test
    fun `should find all`() {
        val localDate = LocalDate.now()
        val amount = 500
        val balance = 600
        val expectedTransactionList = listOf(Transaction(localDate, amount, balance))

        val transactions = Transactions()

        transactions.save(localDate, amount, balance)

        assertContentEquals(expectedTransactionList, transactions.findAll())

    }
}