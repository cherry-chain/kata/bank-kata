package it.cherrychain.kata

import it.cherrychain.kata.domain.Accounts
import it.cherrychain.kata.domain.DepositMoney
import it.cherrychain.kata.domain.WithdrawMoney
import it.cherrychain.kata.view.*


class Bank(
    private val statements: Statements,
    private val deposits: Deposits,
    private val eventbus: Eventbus
) {
    fun depositToAccount(amountToDeposit: Int) {
        eventbus.send(DepositMoney(amountToDeposit))
    }

    fun withdrawFromAccount(amountToWithdraw: Int) {
        eventbus.send(WithdrawMoney(amountToWithdraw))
    }

    fun printStatementOfAccount():String {
        var toPrint :String = "Date          Amount    Balance\n"
        for (statement in statements.findAll()) {
            val sign = if (statement.amount>0) "+" else ""
            toPrint += statement.eventDate.format(formatter)+"     $sign${statement.amount}       ${statement.balance}\n"
        }
        println(toPrint)

        return toPrint
    }

    fun printAllDeposits(): String {

        var toPrint :String = "Date          Amount\n"
        for (deposit in deposits.findAll()) {
            toPrint += deposit.depositedAt.format(formatter)+"     +${deposit.moneyDeposited}\n"
        }
        println(toPrint)

        return toPrint
    }

}

fun main() {
    val statement = Statements(mutableListOf())

}