package it.cherrychain.kata.view

import java.time.LocalDateTime


data class Deposit(val depositedAt:LocalDateTime, val moneyDeposited: Int)
class Deposits(private val depositList : MutableList<Deposit>) {
    fun save(depositedAt: LocalDateTime, depositedAmount: Int): Deposit {
        val deposit = Deposit(depositedAt, depositedAmount)
        depositList += deposit
        return deposit
    }

    fun findAll(): List<Deposit> = depositList.map { it }
    fun findTotalDeposit(): Int {
        return findAll().sumOf { it.moneyDeposited }
    }


}
