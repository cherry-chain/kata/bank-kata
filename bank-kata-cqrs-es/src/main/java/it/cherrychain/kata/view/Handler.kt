package it.cherrychain.kata.view

import it.cherrychain.kata.domain.Handler
import it.cherrychain.kata.domain.MoneyDeposited
import it.cherrychain.kata.domain.MoneyWithdrawn


class MoneyDepositedHandler(private val statements: Statements): Handler {
    override fun handle(event: Any) {
        when (event) {
            is MoneyDeposited -> {
                statements.save(event.depositedAt, event.depositedAmount)
            }
        }
    }
}

class TotalMoneyDepositedHandler(private val deposits: Deposits): Handler {
    override fun handle(event: Any) {
        when (event) {
            is MoneyDeposited -> {
                deposits.save(event.depositedAt, event.depositedAmount)
            }
        }
    }
}


class MoneyWithdrawnHandler(private val statements: Statements): Handler {
    override fun handle(event: Any) {
        when (event) {
            is MoneyWithdrawn -> {
                statements.save(event.withdrawnAt, -event.withdrawnAmount)
            }
        }
    }

}