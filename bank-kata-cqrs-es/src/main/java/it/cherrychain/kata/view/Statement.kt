package it.cherrychain.kata.view

import it.cherrychain.kata.formatter
import java.time.LocalDateTime


data class Statement(val eventDate: LocalDateTime, val amount: Int, val balance: Int)



class Statements(private var statementList :MutableList<Statement>) {

    fun findAll(): List<Statement> = statementList.map { it }

    fun save(eventDate: LocalDateTime, amount: Int):Statement {
        val allStatements = findAll()
        val balance = allStatements.sumOf {it.amount?:0} + amount
        val statement = Statement(eventDate, amount, balance)
        statementList += statement
        return statement
    }

}