package it.cherrychain.kata

import it.cherrychain.kata.domain.Handler
import kotlin.reflect.KClass

class Eventbus(
    private val handlersList: MutableList<Pair<KClass<out Any>, Handler>> = mutableListOf()
) {
    fun send(message:Any) {
        handlersList.forEach { (type, handler) -> if (type == message::class) handler.handle(message) }
    }

    fun register(handlerCouple: Pair<KClass<*>, Handler>) {
        handlersList += handlerCouple
    }

}