package it.cherrychain.kata.domain

data class Account (val balance: Int)

class Accounts(private val eventStore: EventStore) {
    fun generateAccount(): Account {
        val allEvents = eventStore.findAll()
        val balance = allEvents.sumOf {
            when (it.domainEvent) {
                is MoneyDeposited -> it.domainEvent.depositedAmount
                is MoneyWithdrawn -> -it.domainEvent.withdrawnAmount
                else -> 0
            }
        }

        return Account(balance)
    }

    fun save(event: MoneyDeposited) {
        eventStore.save(event)
    }

    fun save(event: MoneyWithdrawn) {
        eventStore.save(event)
    }
}