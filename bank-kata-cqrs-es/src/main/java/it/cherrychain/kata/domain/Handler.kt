package it.cherrychain.kata.domain


data class DepositMoney(val moneyToDeposit:Int)
data class WithdrawMoney(val moneyToWithdraw:Int)

interface Handler {fun handle(command: Any)}
class DepositMoneyHandler(private val accounts: Accounts)  : Handler {
    override fun handle(command: Any) {
        when (command) {
            is DepositMoney -> {
                val moneyDeposited = MoneyDeposited(command.moneyToDeposit)
                accounts.save(moneyDeposited)
            }
        }
    }
}

class WithdrawMoneyHandler(private val accounts: Accounts) : Handler {
    override fun handle(command: Any) {
        when (command) {
            is WithdrawMoney -> {
                val account = accounts.generateAccount()
                val balance = account.balance

                if (balance >= command.moneyToWithdraw) {
                    val moneyWithdrawn = MoneyWithdrawn(command.moneyToWithdraw)
                    accounts.save(moneyWithdrawn)
                }
            }
        }
    }
}