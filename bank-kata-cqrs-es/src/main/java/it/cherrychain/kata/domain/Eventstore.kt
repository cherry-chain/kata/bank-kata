package it.cherrychain.kata.domain

import it.cherrychain.kata.Eventbus
import java.time.LocalDateTime

data class MoneyDeposited(val depositedAmount:Int, val depositedAt: LocalDateTime = LocalDateTime.now())
data class MoneyWithdrawn(val withdrawnAmount:Int, val withdrawnAt: LocalDateTime = LocalDateTime.now())
data class Event(val domainEvent: Any, val createdAt: LocalDateTime = LocalDateTime.now())

class EventStore(private var eventList :List<Event> = mutableListOf<Event>(), private val eventbus: Eventbus) {
    fun findAll(): List<Event> = eventList.map { it }

    fun save(domainEvent: Any): Event {
        val eventToBeSaved = Event(domainEvent)
        eventList += eventToBeSaved
        eventbus.send(domainEvent)
        return eventToBeSaved
    }


}