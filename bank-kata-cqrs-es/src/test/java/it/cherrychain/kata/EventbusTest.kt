package it.cherrychain.kata

import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import it.cherrychain.kata.domain.*
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import kotlin.reflect.KClass

class EventbusTest {

    private val depositMoneyHandler = mockk<DepositMoneyHandler>()
    private val withdrawMoneyHandler = mockk<WithdrawMoneyHandler>()
    @Test
    fun `should send message`() {
        val deposit = DepositMoney(500)
        val withdrawal = WithdrawMoney(100)

        val eventbus = Eventbus(
            mutableListOf(
            DepositMoney::class to depositMoneyHandler,
            WithdrawMoney::class to withdrawMoneyHandler
        ))

        every { depositMoneyHandler.handle(deposit) } returns Unit
        every { withdrawMoneyHandler.handle(withdrawal) } returns Unit


        eventbus.send(deposit)
        eventbus.send(withdrawal)

        verify { depositMoneyHandler.handle(deposit) }
        verify { withdrawMoneyHandler.handle(withdrawal) }
        verify(exactly = 0) {withdrawMoneyHandler.handle(deposit)  }
        verify(exactly = 0) {depositMoneyHandler.handle(withdrawal)  }

    }

    @Test
    fun `should add handler to eventbus`() {

        val deposit = DepositMoney(500)
        val listOf = mutableListOf<Pair<KClass<*>, Handler>>()
        val eventbus = Eventbus(listOf)
        val handlerCouple = DepositMoney::class to depositMoneyHandler

        eventbus.register(handlerCouple)

        Assertions.assertThat(listOf).containsOnly(handlerCouple)
    }
}