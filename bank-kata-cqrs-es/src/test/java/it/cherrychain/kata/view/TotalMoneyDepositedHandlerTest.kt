package it.cherrychain.kata.view

import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import it.cherrychain.kata.domain.MoneyDeposited
import org.junit.jupiter.api.Test

class TotalMoneyDepositedHandlerTest{

    val deposits = mockk<Deposits>()

    @Test
    fun `should save deposit`() {
        val totalMoneyDepositedHandler = TotalMoneyDepositedHandler(deposits)
        val event = MoneyDeposited(500)
        val deposit = Deposit(event.depositedAt, event.depositedAmount)

        every { deposits.save(event.depositedAt, event.depositedAmount) } returns deposit

        totalMoneyDepositedHandler.handle(event)

        verify { deposits.save(event.depositedAt, event.depositedAmount) }

    }
}