package it.cherrychain.kata.view

import it.cherrychain.kata.domain.MoneyDeposited
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test

class DepositsTest {

    @Test
    fun `should save deposit in list`() {
        val deposits = Deposits(mutableListOf())
        val moneyDeposited = MoneyDeposited(500)
        val deposit = Deposit(moneyDeposited.depositedAt, moneyDeposited.depositedAmount)

        Assertions.assertThat(deposits.save(moneyDeposited.depositedAt, moneyDeposited.depositedAmount)).isEqualTo(deposit)

    }

    @Test
    fun `should find all deposits`() {
        val moneyDeposited = MoneyDeposited(500)
        val deposit = Deposit(moneyDeposited.depositedAt, moneyDeposited.depositedAmount)
        val deposits = Deposits(mutableListOf(deposit))

        Assertions.assertThat(deposits.findAll()).containsOnly(deposit)

    }

    @Test
    fun `should return total deposit`() {
        val moneyDeposited1 = MoneyDeposited(500)
        val moneyDeposited2 = MoneyDeposited(600)
        val deposit1 = Deposit(moneyDeposited1.depositedAt, moneyDeposited1.depositedAmount)
        val deposit2 = Deposit(moneyDeposited2.depositedAt, moneyDeposited2.depositedAmount)
        val deposits = Deposits(mutableListOf(deposit1, deposit2))

        Assertions.assertThat(deposits.findTotalDeposit()).isEqualTo(1100)
    }
}