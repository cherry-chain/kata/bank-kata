package it.cherrychain.kata.view

import it.cherrychain.kata.domain.MoneyDeposited
import it.cherrychain.kata.domain.MoneyWithdrawn
import it.cherrychain.kata.formatter
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test

class StatementsTest {

    @Test
    fun `should add statement to statement list`() {

        val statements = Statements(mutableListOf())
        val moneyDeposited = MoneyDeposited(500)
        val statement = Statement(moneyDeposited.depositedAt, moneyDeposited.depositedAmount, 500)

        Assertions.assertThat(statements.save(moneyDeposited.depositedAt, moneyDeposited.depositedAmount)).isEqualTo(statement)
    }

    @Test
    fun `should find all statements`() {

        val moneyDeposited = MoneyDeposited(500)
        val statement = Statement(moneyDeposited.depositedAt, moneyDeposited.depositedAmount, 500)
        val statements  = Statements(mutableListOf(statement))
        Assertions.assertThat(statements.findAll()).containsOnly(statement)
    }
}