package it.cherrychain.kata.view

import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import it.cherrychain.kata.domain.MoneyDeposited
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class MoneyDepositedHandlerTest{
    val statements = mockk<Statements>()

    @Test
    fun `should save statement`() {
        val moneyDepositedHandler = MoneyDepositedHandler(statements)
        val event = MoneyDeposited(500)
        val statement = Statement(event.depositedAt, event.depositedAmount, 500)

        every { statements.save(event.depositedAt, event.depositedAmount) } returns statement

        moneyDepositedHandler.handle(event)

        verify { statements.save(event.depositedAt, event.depositedAmount) }

    }

}