package it.cherrychain.kata.view

import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import it.cherrychain.kata.domain.MoneyWithdrawn
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class MoneyWithdrawnHandlerTest{
    val statements = mockk<Statements>()

    @Test
    fun `should save statement`() {
        val moneyWithdrawnHandler= MoneyWithdrawnHandler(statements)
        val event = MoneyWithdrawn(100)
        val statement = Statement(event.withdrawnAt, event.withdrawnAmount, 400)

        every { statements.save(event.withdrawnAt, -event.withdrawnAmount) } returns statement

        moneyWithdrawnHandler.handle(event)

        verify { statements.save(event.withdrawnAt, -event.withdrawnAmount) }

    }
}