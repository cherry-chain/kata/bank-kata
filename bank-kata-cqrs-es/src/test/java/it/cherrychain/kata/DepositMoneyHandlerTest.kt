package it.cherrychain.kata

import io.mockk.*
import it.cherrychain.kata.domain.Accounts
import it.cherrychain.kata.domain.DepositMoney
import it.cherrychain.kata.domain.DepositMoneyHandler
import it.cherrychain.kata.domain.MoneyDeposited
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.time.LocalDateTime

@Suppress("UsePropertyAccessSyntax")
class DepositMoneyHandlerTest {

    private val accounts = mockk<Accounts>()

    @BeforeEach
    fun initializeDateTime() {
        val dateTime = LocalDateTime.now()
        mockkStatic(LocalDateTime::class)
        every { LocalDateTime.now() } returns dateTime
    }

    @AfterEach
    fun unmockDateTime() {
        unmockkStatic(LocalDateTime::class)
    }

    @Test
    fun `should deposit to account`() {
        val depositMoneyHandler = DepositMoneyHandler(accounts)

        val depositToAccount = DepositMoney(500)

        val event = MoneyDeposited(500)

        every { accounts.save(event) } returns Unit

        depositMoneyHandler.handle(depositToAccount)

        verify { accounts.save(event) }

    }
}