package it.cherrychain.kata

import io.mockk.*
import it.cherrychain.kata.domain.*
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.time.LocalDateTime

@Suppress("UsePropertyAccessSyntax")
class WithdrawMoneyHandlerTest{

    private val accounts = mockk<Accounts>()

    @BeforeEach
    fun initializeDateTime() {
        val dateTime = LocalDateTime.now()
        mockkStatic(LocalDateTime::class)
        every { LocalDateTime.now() } returns dateTime
    }

    @AfterEach
    fun unmockDateTime() {
        unmockkStatic(LocalDateTime::class)
    }

    @Test
    fun `should withdraw from account`() {
        val withdrawMoneyHandler = WithdrawMoneyHandler(accounts)

        val withdrawFromAccount = WithdrawMoney(100)

        val event = MoneyWithdrawn(100)

        every { accounts.save(event) } returns Unit
        every { accounts.generateAccount() } returns Account(balance = 100)

        withdrawMoneyHandler.handle(withdrawFromAccount)

        verify { accounts.save(event) }
    }

}