package it.cherrychain.kata

import it.cherrychain.kata.domain.*
import it.cherrychain.kata.view.*
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import java.time.LocalDate
import java.time.LocalDateTime

class BankTest {

    @Test
    fun `should deposit and withdraw and then print statement`() {
        val moneyDeposited = MoneyDeposited(500)
        val moneyWithdrawn = MoneyWithdrawn(100)
        val event1 = Event(moneyDeposited, LocalDateTime.now())
        val event2 = Event(moneyWithdrawn, LocalDateTime.now())
        val eventList = listOf<Event>(event1, event2)

        val eventbus = Eventbus()
        val eventStore = EventStore(eventList, eventbus)
        val accounts = Accounts(eventStore)
        val depositMoneyHandler = DepositMoneyHandler(accounts)
        val withdrawMoneyHandler = WithdrawMoneyHandler(accounts)

        eventbus.register(DepositMoney::class to depositMoneyHandler)
        eventbus.register(WithdrawMoney::class to withdrawMoneyHandler)

        val statements = Statements(mutableListOf())
        val deposits = Deposits(mutableListOf())

        val moneyDepositedHandler = MoneyDepositedHandler(statements)
        val moneyWithdrawnHandler = MoneyWithdrawnHandler(statements)
        eventbus.register(MoneyDeposited::class to moneyDepositedHandler)
        eventbus.register(MoneyWithdrawn::class to moneyWithdrawnHandler)


        val bank = Bank(statements, deposits, eventbus)


        val amountToDeposit = 500
        bank.depositToAccount(amountToDeposit)

        val amountToWithdraw = 100
        bank.withdrawFromAccount(amountToWithdraw)

        val localDate = LocalDate.now()

        val expectedStatementString = "Date          Amount    Balance\n" + localDate.format(formatter) + "     +$amountToDeposit       500\n" + localDate.format(
            formatter) + "     -$amountToWithdraw       400\n"


        Assertions.assertThat( bank.printStatementOfAccount() ).isEqualTo(expectedStatementString)
    }

    @Test
    fun `should deposit, withdraw, deposit and then print all deposits`() {
        val moneyDeposited1 = MoneyDeposited(500)
        val moneyWithdrawn = MoneyWithdrawn(100)
        val moneyDeposited2 = MoneyDeposited(600)

        val event1 = Event(moneyDeposited1, LocalDateTime.now())
        val event2 = Event(moneyWithdrawn, LocalDateTime.now())
        val event3 = Event(moneyDeposited2, LocalDateTime.now())
        val eventList = listOf<Event>(event1, event2, event3)

        val eventbus = Eventbus()
        val eventStore = EventStore(eventList, eventbus)
        val accounts = Accounts(eventStore)
        val depositMoneyHandler = DepositMoneyHandler(accounts)
        val withdrawMoneyHandler = WithdrawMoneyHandler(accounts)

        eventbus.register(DepositMoney::class to depositMoneyHandler)
        eventbus.register(WithdrawMoney::class to withdrawMoneyHandler)

        val statements = Statements(mutableListOf())
        val deposits = Deposits(mutableListOf())

        val moneyDepositedHandler = MoneyDepositedHandler(statements)
        val moneyWithdrawnHandler = MoneyWithdrawnHandler(statements)
        val totalMoneyDepositedHandler = TotalMoneyDepositedHandler(deposits)

        eventbus.register(MoneyDeposited::class to moneyDepositedHandler)
        eventbus.register(MoneyWithdrawn::class to moneyWithdrawnHandler)
        eventbus.register(MoneyDeposited::class to totalMoneyDepositedHandler)


        val bank = Bank(statements, deposits, eventbus)


        val amountToDeposit1 = 500
        bank.depositToAccount(amountToDeposit1)


        val amountToWithdraw = 100
        bank.withdrawFromAccount(amountToWithdraw)

        val amountToDeposit2 = 600
        bank.depositToAccount(amountToDeposit2)

        val localDate = LocalDate.now()

        val expectedStatementString = "Date          Amount\n" + localDate.format(formatter) + "     +$amountToDeposit1\n" + localDate.format(formatter) + "     +$amountToDeposit2\n"


        Assertions.assertThat( bank.printAllDeposits() ).isEqualTo(expectedStatementString)
    }
}