package it.cherrychain.kata

import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import it.cherrychain.kata.domain.*
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test

class AccountsTest{

    private val eventStore = mockk<EventStore>()
    @Test
    fun `should generate account`() {

        val moneyDeposited = MoneyDeposited(500)
        val moneyWithdrawn = MoneyWithdrawn(100)
        val event1 = Event(moneyDeposited)
        val event2 = Event(moneyWithdrawn)
        every { eventStore.findAll() } returns listOf(event1, event2)

        val accounts = Accounts(eventStore)
        val account = accounts.generateAccount()

        verify { eventStore.findAll() }
        Assertions.assertThat(account).isEqualTo(Account(400))


    }
}