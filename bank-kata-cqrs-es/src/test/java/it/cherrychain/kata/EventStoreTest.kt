package it.cherrychain.kata

import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import it.cherrychain.kata.domain.MoneyDeposited
import it.cherrychain.kata.domain.Event
import it.cherrychain.kata.domain.EventStore
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test

class EventStoreTest {

    val eventbus = mockk<Eventbus>()
    @Test
    fun `should save event`() {
        val moneyDeposited = MoneyDeposited(100)
        val expectedEvent = Event(moneyDeposited)

        val events = EventStore(listOf(), eventbus)
        every { eventbus.send(moneyDeposited) } returns Unit

        Assertions.assertThat(events.save(moneyDeposited)).matches { it.domainEvent == expectedEvent.domainEvent && it.createdAt > expectedEvent.createdAt }
    }

    @Test
    fun `should find all events`() {
        val moneyDeposited = MoneyDeposited(100)
        val event = Event(moneyDeposited)

        val events = EventStore(listOf(event), eventbus)

        Assertions.assertThat(events.findAll()).containsOnly(event)
    }

    @Test
    fun `should send event when saved`() {
        val moneyDeposited = MoneyDeposited(100)

        val events = EventStore(listOf(), eventbus)

        every { eventbus.send(moneyDeposited) } returns Unit

        events.save(moneyDeposited)

        verify { eventbus.send(moneyDeposited) }
    }
}